<?php

/**
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp;

use SalesDepartmentApp\Entity\PaymentDate;
use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use SalesDepartmentApp\Helper\FileHelper;

class Generator
{

    /**
     * @var string $filename
     */
    public $filename;

    /**
     * Generator constructor.
     *
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Save payment details for given month
     *
     * @param \DateTime $month
     *
     * @return void
     */
    public function create($month)
    {
        $config = new ExporterConfig();
        $config
            ->setDelimiter("\t")
            ->setEnclosure("'")
            ->setFromCharset('UTF-8')
            ->setFileMode('a+') // Customize file mode
        ;
        $exporter = new Exporter($config);

        $rows = array();

        if(false === FileHelper::isExistFileInPhar($this->filename)) {
            $rows[] = array('Month', 'Salary', 'Bonus');
        }

        $paymentDate = new PaymentDate($month);
        $rows[] = array($paymentDate->getMonth(), $paymentDate->getSalaryPaymentDate(), $paymentDate->getBonusPaymentDate());

        $exporter->export($this->filename, $rows);

    }
}