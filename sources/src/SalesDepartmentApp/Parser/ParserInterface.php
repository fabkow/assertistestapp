<?php
/**
 * The ParserInterface class
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Parser;

interface ParserInterface
{
    /**
     * ParserInterface constructor.
     * @param $array|null
     */
    public function __construct($data);

    /**
     * @return \DateTime
     */
    public function getLatestDate();
}