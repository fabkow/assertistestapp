<?php
/**
 * Parser file
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Parser;

abstract class Parser
{
    /**
     * @var array $data
     */
    protected $data;

    /**
     * {@inheritdoc}
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}