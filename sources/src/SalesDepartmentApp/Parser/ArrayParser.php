<?php

/**
 * Parse file
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Parser;

class ArrayParser extends Parser implements ParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLatestDate()
    {
        $defaultDate = new \DateTime();

        if (empty($this->data)) {
            return $defaultDate;
        }

        $lastRow = end($this->data);
        if (!isset($lastRow['month'])) {
            return $defaultDate;
        }

        return \DateTime::createFromFormat('Y-m', $lastRow['month']);
    }
}