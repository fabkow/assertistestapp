<?php
/**
 * The ParserLoaderInterface class
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Parser;

interface ParserLoaderInterface
{
    /**
     * ParserLoaderInterface constructor.
     * @param mixed
     */
    public function load($data);
}