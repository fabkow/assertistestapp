<?php

/**
 * The ParserLoader class
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Parser;

/**
 * Class ParserLoader
 *
 * Class load appropriate parser
 *
 * @package SalesDepartmentApp\Parser
 */
class ParserLoader implements ParserLoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function load($dataType)
    {
        switch ($dataType) {
            case (is_array($dataType)):
            default:
                return new ArrayParser($dataType);
                break;
        }}
}