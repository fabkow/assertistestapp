<?php

/**
 * PaymentDate Entity
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Entity;

class PaymentDate
{
    /**
     * @var \DateTime $month
     */
    private $month;

    /**
     * @var \DateTime $salaryPaymentDate
     */
    private $salaryPaymentDate;

    /**
     * @var \DateTime $bonusPaymentDate
     */
    private $bonusPaymentDate;

    /**
     * PaymentDate constructor.
     *
     * @param \DateTime $month
     */
    public function __construct(\DateTime $month)
    {
        $this->month = $month;
        $this->setSalaryPaymentDate();
        $this->setBonusPaymentDate();
    }

    /**
     * @return string
     */
    public function getMonth()
    {
        return $this->month->format('Y-m');
    }

    /**
     * @return string
     */
    public function getSalaryPaymentDate()
    {
        return $this->salaryPaymentDate->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getBonusPaymentDate()
    {
        return $this->bonusPaymentDate->format('Y-m-d');
    }

    private function setSalaryPaymentDate()
    {
        /** @var \DateTime $salaryDay */
        $salaryDay = clone $this->month;
        $salaryDay->modify('last day of this month');

        if (!$this->isWeekday($salaryDay)) {
            $salaryDay->modify('last friday');
        }

        $this->salaryPaymentDate = $salaryDay;
    }

    private function setBonusPaymentDate()
    {
        /** @var \DateTime $bonusDay */
        $bonusDay = clone $this->month;
        $bonusDay->modify($bonusDay->format('Y-m-15'));

        if (!$this->isWeekday($bonusDay)) {
            $bonusDay->modify('next wednesday');
        }

        $this->bonusPaymentDate = $bonusDay;
    }

    /**
     * Function return true if date is weekday
     *
     * @param \DateTime $date
     *
     * @return bool
     */
    private function isWeekday(\DateTime $date)
    {
        $dayOfWeek = $date->format('N'); //ISO-8601 numeric representation of the day of the week

        return ($dayOfWeek >= 6) ? false : true;
    }
}