<?php

namespace SalesDepartmentApp\Helper;

/**
 * The FileHelper class
 *
 * @author Fabian Kowalczyk <fabkow@gmail.com>
 */
class FileHelper
{
    /**
     * Check file exists (in phar)
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function isExistFileInPhar($filename)
    {
        $pharFile = \Phar::running(false);
        if (file_exists(('' === $pharFile ? '' : dirname($pharFile) . DIRECTORY_SEPARATOR) . $filename)) {
            $return = true;
        } else {
            $return = false;
        }

        return $return;
    }
}