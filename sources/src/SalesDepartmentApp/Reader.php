<?php

/**
 * Read file
 *
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp;

use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use SalesDepartmentApp\Helper\FileHelper;

class Reader
{
    /**
     * @var string $filename
     */
    public $filename;

    /**
     * Reader constructor.
     *
     * @param string $filename
     *
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Read file with data
     *
     * @return array
     */
    public function get() {

        $data = array();

        if(false === FileHelper::isExistFileInPhar($this->filename)) {
            return $data;
        }

        $config = new LexerConfig();
        $config
            ->setDelimiter("\t")
            ->setEnclosure("'");
        $lexer = new Lexer($config);

        $interpreter = new Interpreter();
        $interpreter->addObserver(function(array $row) use (&$data) {
            $data[] = array(
                'month'      => $row[0],
                'salary'     => $row[1],
                'bonus'      => $row[2],
            );
        });

        $lexer->parse($this->filename, $interpreter);

        return $data;
    }

}