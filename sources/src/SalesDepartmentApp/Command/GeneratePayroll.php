<?php

/**
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

namespace SalesDepartmentApp\Command;

use SalesDepartmentApp\Generator;
use SalesDepartmentApp\Parser\ParserLoader;
use SalesDepartmentApp\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GeneratePayroll extends Command
{
    const PAYROLL_END_DATE = '2017-12';

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate-payroll')
            ->setDescription('Generate payroll')
            ->setHelp('This command allows you to generate payroll')
            ->addArgument('filename', InputArgument::REQUIRED, 'Filename to read');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('filename');
        $endDate = \DateTime::createFromFormat('Y-m', self::PAYROLL_END_DATE);

        $parserLoader = new ParserLoader();
        $parser = $parserLoader->load($this->readData($file));

        while ($parser->getLatestDate() < $endDate) {
            $latestDateFromFile = $parser->getLatestDate();
            $nextMonth = $latestDateFromFile->modify('first day of next month');

            $output->writeln(sprintf('Generate payroll for %s', $nextMonth->format('Y-m')));
            $generator = new Generator($file);
            $generator->create($nextMonth);

            $parser = $parserLoader->load($this->readData($file));
        }

        $output->writeln('File has processed all months.');
    }

    /**
     * @param string $file
     *
     * @return array
     */
    private function readData($file)
    {
        $reader = new Reader($file);
        $data = $reader->get();

        return $data;
    }
}