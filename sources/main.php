<?php

/**
 * Copyright (c) 2017 Fabian Kowalczyk <fabkow@gmail.com>
 */

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new \SalesDepartmentApp\Command\GeneratePayroll());

$application->run();