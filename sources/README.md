# Sales Department App #

## Description ##
Application generates payroll until the end of 2016 year.

## Usuage ##
In CLI just type name of file:
```
php main.php app:generate-payroll <filename.csv>
```
or
```
php app.phar app:generate-payroll <filename.csv>
```

New file is saving in the same directory

## Testing ##
Run phpunit
