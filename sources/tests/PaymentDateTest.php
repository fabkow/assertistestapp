<?php

namespace SalesDepartmentAppTest;

/**
 * Class PaymentDateTest
 *
 * @package SalesDepartmentAppTest
 *
 * @author Fabian Kowalczyk <fabkow@gmail.com>
 */
class PaymentDateTest extends \PHPUnit_Framework_TestCase
{
    public function testPaymentDateReturnValidSalaryDay()
    {
        $testDate = new \DateTime('2017-09-10');
        $paymentDate = new \SalesDepartmentApp\Entity\PaymentDate($testDate);

        $this->assertEquals('2017-09-29', $paymentDate->getSalaryPaymentDate());
    }

    public function testPaymentDateReturnValidBonusDay()
    {
        $testDate = new \DateTime('2017-07-10');
        $paymentDate = new \SalesDepartmentApp\Entity\PaymentDate($testDate);

        $this->assertEquals('2017-07-19', $paymentDate->getBonusPaymentDate());
    }
}
