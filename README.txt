Aplikacja do generowania listy dni w kt�rych ma by� wyp�acana pensja oraz premia.

1. Zgodnie z za�o�eniami aplikacja generuje payroll tylko do ko�ca roku (w kodzie jest sta�a kt�ra obecnie zawiera string: 2017-12)
2. Przesy�am 2 wersje:
- wersja �r�d�owa
- wersja phar
3. Wersja phar (zawiera r�wnie� PHP) mo�emy odpali� z PHP 5.3.29 wsz�dzie przechodz�c do katalogu z phar i podaj�c w konsoli: 
php\php.exe app.phar app:generate-payroll payroll.csv
4. Dodatkowe informacje w pliku README.md w folderze z projektem
5. Napisane zosta�y testy jednostkowe sprawdzaj�ce poprawno�� generowania dnia wyp�aty premii i wynagrodzenia, aby uruchomi� w katalogu z kodem �r�d�owym: 
phpunit
